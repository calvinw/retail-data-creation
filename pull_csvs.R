
#main file is called Spreadsheet Master - Retail Data

downloadFile<-function(name, gid, columnClasses=NULL) {
  documentURL<-"https://docs.google.com/spreadsheets/d/e/2PACX-1vQYqrWWShBXbwCA-IZK0HmZ8hKRzWxUZD7y1zMNbIYvmYEgA77lVGID0TZIXMNZ_X2xEs2MzCXpgW9Z"

  csvURL<-paste0(documentURL, "/pub?", "gid=", gid, "&single=true&output=csv")
  download.file(csvURL, destfile=name, method="auto")
}


downloadFile("SuperStyleTemplateByLocationByWeekYear1.csv",679031435)
downloadFile("SuperStyleTemplateByLocationByWeekYear2.csv",1102471088)
downloadFile("TemplateByLocationByWeekYear1.csv", 0)
downloadFile("TemplateByLocationByWeekYear2.csv", 1479789344)
downloadFile("ColorProbabilitiesAll.csv", 1106721688)
downloadFile("SizeProbabilitiesAll.csv", 900935938)
downloadFile("StyleToStyleInfo.csv", 652589989)
downloadFile("ColorToColorNbr.csv", 396500577)
downloadFile("SizeToSizeNbr.csv",1454756339)
downloadFile("LocationToGrade.csv",2025950707)

