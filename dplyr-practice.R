#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly = TRUE)

inputfile<-args[1]
outputfile<-args[2]

data<-read.csv(inputfile, check.names=FALSE)

#str(data)

library(dplyr)

#newdf<-data %>% count(Dept, Class, ClassNbr, Style, StyleNumber, SizeScaling, Region, Location, Week, Ago, Size, SizeNbr, Color, ColorNbr, name="Units")

newdf<-data %>%
    group_by(Size) %>%
     tally()

newdf

#write.csv(newdf, outputfile, quote=FALSE, row.names=FALSE)
