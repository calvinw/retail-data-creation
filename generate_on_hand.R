#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly = TRUE)

style_name<-args[1]
print(paste0("style_name: ", style_name))

filename_base<-gsub(" ","", style_name)

if(startsWith(style_name, "Super Style")) {
     print("Super Style year 2");
     outputfile<-paste0(filename_base, "Coh.csv")
     stores_df<-read.csv("SuperStyleTemplateByLocationByWeekYear2.csv", check.names=FALSE)
} else {
     print("Regular year 2");
     outputfile<-paste0(filename_base, "Coh.csv")
     stores_df<-read.csv("TemplateByLocationByWeekYear2.csv", check.names=FALSE)
}

print(outputfile)

SCALE_FACTOR_COH<-.15

loc_to_grade_df<-read.csv("LocationToGrade.csv", check.names=FALSE)
color_to_colorNbr_df<-read.csv("ColorToColorNbr.csv", check.names=FALSE)
size_to_sizeNbr_df<-read.csv("SizeToSizeNbr.csv", colClasses=rep("character", times=3), check.names=FALSE)
style_to_styleInfo_df<-read.csv("StyleToStyleInfo.csv", check.names=FALSE)
all_color_probs_df<-read.csv("ColorProbabilitiesAll.csv")
all_size_probs_df<-read.csv("SizeProbabilitiesAll.csv")

str(loc_to_grade_df)
str(color_to_colorNbr_df)
str(size_to_sizeNbr_df)
str(style_to_styleInfo_df)
str(all_color_probs_df)
str(all_size_probs_df)

names<-names(stores_df)


#Dept,Class,StyleDesc,SizeScaling,Region,Locations,Indices,Average,COH
#This are the column vectors for the individual records generated
dept<-c()
class<-c()
classNbr<-c()
style<-c()
styleNumber<-c()
scaling<-c()
region<-c()
location<-c()
size<-c()
sizeNbr<-c()
color<-c()
colorNbr<-c()

# i iterates over stores

for(i in 1:26) {
	# Get the size scaling for this store
	SizeScaling<-stores_df$SizeScaling[i]
  LocationName<-stores_df$Location[i]
  StyleName<-stores_df[i,3]

	if(SizeScaling == "Large")
		size_distribution<-large_size_distribution
	else if(SizeScaling == "Normal")
		size_distribution<-normal_size_distribution
	else
		size_distribution<-small_size_distribution

    storename<-stores_df[i,6]

    # Get the number of OH for the current week (11/13).
    on_hand<-stores_df[i,8]

    if(storename == "108 Daytona") {
        on_hand<-round(.10*on_hand)
    } else if(storename == "107 Malibu") {
        on_hand<-round(.20*on_hand)
    } else {
      on_hand<-round(SCALE_FACTOR_COH*on_hand)
    }

    #dept numbers
    depts<-rep(stores_df[i,1], on_hand)
    dept<-c(dept, depts)

    #class names
    className<-stores_df[i,2]
    classes<-rep(className, on_hand)
    class<-c(class, classes)

    #class numbers
    classNumbers<-rep(ClassNbr[[className]], on_hand)
    classNbr<-c(classNbr, classNumbers)

    #style names
    style_name<-stores_df[i,3]
    styles<-rep(style_name, on_hand)
    style<-c(style, styles)

    #style numbers
    styleNumbers<-rep(StyleNumber[[style_name]], on_hand)
    styleNumber<-c(styleNumber, styleNumbers)

    #scaling names
    scalings<-rep(stores_df[i,4], on_hand)
    scaling<-c(scaling, scalings)

    #region names
    regions<-rep(stores_df[i,5], on_hand)
    region<-c(region, regions)

    #choose the sizes for this week
    sizes<-sample(size_distribution, on_hand, replace=TRUE)
    size<-c(size, sizes)

    #lookup the size numbers for the sizes
    sizeNumbers<-sapply(sizes, function(size) {
        return (SizeNumber[[size]])
    })
    sizeNbr<-c(sizeNbr, sizeNumbers)

    #choose the colors for this week
    colors<-sample(color_distribution, on_hand, replace=TRUE)
    color<-c(color, colors)

    #lookup the color numbers for the colors
    colorNumbers<-sapply(colors, function(color) {
        return (ColorNbr[[color]])
    })
    colorNbr<-c(colorNbr, colorNumbers)

    #location names, they are all the same for this week.
    storename<-stores_df[i,6]
    locations<-rep(stores_df[i,6], on_hand)
    location<- c(location, locations)

    # cat("    \n")
    # cat("COH", "  ", StyleName, "  ", LocationName,"\n")
    # cat("    \n")
    # cat("SIZES\n")
    # cat("    \n")
    # print(summary(as.factor(sizes)))
    # cat("    \n")
    # cat("COLORS\n")
    # cat("    \n")
    # print(summary(as.factor(colors)))
}

dept<-c()
class<-c()
classNbr<-c()
style<-c()
styleNumber<-c()
scaling<-c()
region<-c()
location<-c()
size<-c()
sizeNbr<-c()
color<-c()
colorNbr<-c()

records_df<-data.frame(Dept=dept,
		       Class=class,
		       ClassNbr=classNbr,
		       Style=style,
		       StyleNumber=styleNumber,
		       SizeScaling=scaling,
		       Region=region,
		       Location=location,
		       Size=size,
               SizeNbr=sizeNbr,
		       Color=color,
               ColorNbr=colorNbr,
               stringsAsFactors=TRUE)

# str(records_df)
#
# head(records_df, 20)

write.csv(records_df,
	  outputfile,
	  quote=FALSE,
	  row.names=FALSE)

