
#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly = TRUE)

nameOfAllRecords<-args[1]
pattern<-args[2]

#print(pattern)

filenames <- list.files(".", pattern=pattern, full.names=TRUE)
header<-NULL
vectorOfLines<-sapply(filenames, function(name) {
      lines<-readLines(name)
      header<<-lines[1]
      lines<-lines[2:length(lines)]
      return (lines)
})

allLines<-unlist(vectorOfLines)
cat(header, allLines, file=nameOfAllRecords, sep="\n")

