Rscript pull_csvs.R

rm -rf *Records*

Rscript generate_records.R 'Drawstring Jogger' year1
Rscript generate_records.R 'Drawstring Jogger' year2
Rscript generate_records.R 'Belted Wide Leg Twill' year1
Rscript generate_records.R 'Belted Wide Leg Twill' year2
Rscript generate_records.R 'Corduroy Wide Leg' year1
Rscript generate_records.R 'Corduroy Wide Leg' year2
Rscript generate_records.R 'Low Rise Cargo' year1
Rscript generate_records.R 'Low Rise Cargo' year2
Rscript generate_records.R 'Chelsea Boot' year1
Rscript generate_records.R 'Chelsea Boot' year2
Rscript generate_records.R 'Dress Chunky Heel Suede Ankle Boot' year1
Rscript generate_records.R 'Dress Chunky Heel Suede Ankle Boot' year2
Rscript generate_records.R 'Side Zip Bootie' year1
Rscript generate_records.R 'Side Zip Bootie' year2
Rscript generate_records.R 'Suede Chukka Boot' year1
Rscript generate_records.R 'Suede Chukka Boot' year2
Rscript generate_records.R 'Western Bootie' year1
Rscript generate_records.R 'Western Bootie' year2
Rscript generate_records.R 'Pattern Ruched Yoke Ruffle Cuff' year1
Rscript generate_records.R 'Pattern Ruched Yoke Ruffle Cuff' year2
Rscript generate_records.R 'Solid Oversized Shirt' year1
Rscript generate_records.R 'Solid Oversized Shirt' year2
Rscript generate_records.R 'Solid Ruched Yoke Ruffle Cuff' year1
Rscript generate_records.R 'Solid Ruched Yoke Ruffle Cuff' year2
Rscript generate_records.R 'Striped Oversized Shirt' year1
Rscript generate_records.R 'Striped Oversized Shirt' year2

#cat *Records* > allSalesNoSuperStyle.csv
head -q -n 1 DrawstringJoggerRecordsYear1.csv > allSalesNoSuperStyle.csv
tail -q -n +2 *Records* >> allSalesNoSuperStyle.csv

Rscript aggregate.R allSalesNoSuperStyle.csv allAggregatedSalesNoSuperStyle.csv

Rscript generate_records.R 'Super Style Boots' year1
Rscript generate_records.R 'Super Style Boots' year2
Rscript generate_records.R 'Super Style Shirts' year1
Rscript generate_records.R 'Super Style Shirts' year2
Rscript generate_records.R 'Super Style Pants' year1
Rscript generate_records.R 'Super Style Pants' year2

#cat *Records* > allSales.csv
head -q -n 1 DrawstringJoggerRecordsYear1.csv > allSales.csv
tail -q -n +2 *Records* >> allSales.csv

Rscript aggregate.R allSales.csv allAggregatedSales.csv

rm -rf *Coh*

Rscript generate_coh.R 'Drawstring Jogger'
Rscript generate_coh.R 'Belted Wide Leg Twill'
Rscript generate_coh.R 'Corduroy Wide Leg'
Rscript generate_coh.R 'Low Rise Cargo'
Rscript generate_coh.R 'Chelsea Boot'
Rscript generate_coh.R 'Dress Chunky Heel Suede Ankle Boot'
Rscript generate_coh.R 'Side Zip Bootie'
Rscript generate_coh.R 'Suede Chukka Boot'
Rscript generate_coh.R 'Western Bootie'
Rscript generate_coh.R 'Pattern Ruched Yoke Ruffle Cuff'
Rscript generate_coh.R 'Solid Oversized Shirt'
Rscript generate_coh.R 'Solid Ruched Yoke Ruffle Cuff'
Rscript generate_coh.R 'Striped Oversized Shirt'

#cat *Coh* > allCohNoSuperStyle.csv
head -q -n 1 DrawstringJoggerCoh.csv > allCurrentOnHandNoSuperStyle.csv
tail -q -n +2 *Coh* >> allCurrentOnHandNoSuperStyle.csv

Rscript aggregate_oh.R allCurrentOnHandNoSuperStyle.csv allAggregatedCurrentOnHandNoSuperStyle.csv

Rscript generate_coh.R 'Super Style Boots'
Rscript generate_coh.R 'Super Style Shirts'
Rscript generate_coh.R 'Super Style Pants'

#cat *Coh* > allCoh.csv
head -q -n 1 DrawstringJoggerCoh.csv > allCurrentOnHand.csv
tail -q -n +2 *Coh* >> allCurrentOnHand.csv

Rscript aggregate_oh.R allCurrentOnHand.csv allAggregatedCurrentOnHand.csv
