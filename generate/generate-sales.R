
makeDfForCSV<-function(gid, columnClasses=NULL) {

  documentURL<-"https://docs.google.com/spreadsheets/d/e/2PACX-1vQYqrWWShBXbwCA-IZK0HmZ8hKRzWxUZD7y1zMNbIYvmYEgA77lVGID0TZIXMNZ_X2xEs2MzCXpgW9Z"

  csvURL<-paste0(documentURL, "/pub?", "gid=", gid, "&single=true&output=csv")

  if(is.null(columnClasses))
    df<-read.csv(csvURL, check.names=F)
  else
    df<-read.csv(csvURL, colClasses=columnClasses, check.names=F)

  return (df)
}

Year1Df<-makeDfForCSV(0)
str(Year1Df)

Year2Df<-makeDfForCSV(1479789344)
str(Year2Df)

colorProbDf<-makeDfForCSV(1106721688)
str(colorProbDf)

sizeProbDf<-makeDfForCSV(900935938)
str(sizeProbDf)

styleToStyleNbrDf<-makeDfForCSV(652589989)
str(styleToStyleNbrDf)

colorToColorNbrDf<-makeDfForCSV(396500577)
str(colorToColorNbrDf)

sizeToSizeNbrDf<-makeDfForCSV(1454756339,
                              columnClasses=rep("character", times=3))
str(sizeToSizeNbrDf)

locationToGradeDf<-makeDfForCSV(2025950707)
str(locationToGradeDf)

locationToSizeDf<-makeDfForCSV(345716216)
str(locationToSizeDf)
