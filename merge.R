# Sample data with store names
df1 <- data.frame(Store = c("Daytona", "Newark", "Chicago", "Miami", "Denver"),
                  Sales = c(100, 200, 150, 120, 180))

print("origininal")
print(df1)

df2 <- data.frame(Store = c("Daytona", "Newark", "Chicago", "Miami", "Denver"),
                  Grade = c("A", "B", "C", "D", "A"))

# Merge dataframes based on the "Store" column
merged_df <- merge(df1, df2, by = "Store")

print("merged")
print(merged_df)

# Filter out rows where Grade is "D"
filtered_df <- merged_df[merged_df$Grade != "D", ]

filtered_df$Grade<-NULL

# Print the result
print("filtered_df")
print(filtered_df)
